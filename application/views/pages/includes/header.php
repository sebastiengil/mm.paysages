
<?php ?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <title>MMPaysages</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    

    <link href="/assets/home.css">
  </head>

  <body>

    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark" >
      <a class="navbar-brand" href="home" style="color:yellow">MMPaysages</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="home" style="color:yellow">Acceuil <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="about" style="color:yellow">Mes projets</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="conseils" style="color:yellow">Conseil du mois</a>
          </li>
		      <li class="nav-item">
            <a class="nav-link" href="contact" style="color:yellow">Contact</a>
          </li>
          <!-- Example split danger button -->
<div class="btn-group">
  <button type="button" class="btn btn-danger">Admin</button>
  <button type="button" class="btn btn-danger dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <span class="sr-only">Toggle Dropdown</span>
  </button>
  <div class="dropdown-menu">
    <a class="dropdown-item" href="admin_conseil">Admin Conseils</a>
    <a class="dropdown-item" href="admin_projet">Admin Projets</a>

  </div>
</div>
         </ul>
        
        <form class="form-inline my-2 my-lg-0 pull-right">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit"><a href='http://mm.paysages.bwb/form' style="color:yellow">Se connecter</a></button>
        </form>
      </div>
    </nav>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
   
    <script src="assets/js/bootstrap.min.js"></script>
    <script>$('.dropdown-toggle').dropdown()</script>