<?php ?>
<link href="/assets/home.css">


<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner" role="listbox">
      <!-- Slide One - Set the background image for this slide in the line below -->
      <div class="carousel-item active" style="background-image: url('https://cdn.pixabay.com/photo/2018/05/18/01/01/urban-park-3410146_960_720.jpg')">
        <div class="carousel-caption d-none d-md-block">
          <h2 class="display-4" style="color:yellow">MM.Paysages</h2>
          <p class="lead"  style="color:yellow">Création paysagère</p>
        </div>
      </div>
      <!-- Slide Two - Set the background image for this slide in the line below -->
      <div class="carousel-item" style="background-image: url('https://image.shutterstock.com/image-photo/man-trimming-hedge-trimmer-machine-600w-146686502.jpg')">
        <div class="carousel-caption d-none d-md-block">
          <h2 class="display-4" style="color:yellow">MM.Paysages</h2>
          <p class="lead" style="color:yellow">Entretien/élagage.</p>
        </div>
      </div>
      <!-- Slide Three - Set the background image for this slide in the line below -->
      <div class="carousel-item" style="background-image: url('https://image.shutterstock.com/image-photo/gardening-tools-on-wooden-table-600w-1054756652.jpg')">
        <div class="carousel-caption d-none d-md-block">
          <h2 class="display-4" style="color:yellow">MM.Paysages</h2>
          <p class="lead" style="color:yellow">Arrosage.</p>
        </div>
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only" style="color:yellow">Previous</span>
        </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only" style="color:yellow">Next</span>
        </a>
  </div>
  <style> 
  .carousel-item {
  height: 100vh;
  min-height: 350px;
  background: no-repeat center center scroll;
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
}
  </style>
 



 
