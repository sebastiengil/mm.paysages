<?php ?>
    <main role="main">
<div style="background-image:url(/assets/images/mmpaysages1.jpg);">

      <!-- Main jumbotron for a primary marketing message or call to action -->
      <div class="jumbotron">
        <div class="container">
          <h2 class="display-3">MM.Paysages</h2>
        
      </div>

      <!-- Page Content -->
    <div class="container">

<hr>

<!-- Project Three -->
<div class="row">
<div class="col-md-12">
<?php foreach ($query as $row): ?>
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner" role="listbox">
      <!-- Slide One - Set the background image for this slide in the line below -->
      <div class="carousel-item active" style="background-image: url('http://placehold.it/700x300')">
        <div class="carousel-caption d-none d-md-block">
          <h2 class="display-4">MM.Paysages</h2>
          <p class="lead">Création paysagère</p>
        </div>
      </div>
      <!-- Slide Two - Set the background image for this slide in the line below -->
      <div class="carousel-item" style="background-image: url('https://cdn.pixabay.com/photo/2014/02/16/20/32/iceland-267647__340.jpg')">
        <div class="carousel-caption d-none d-md-block">
          <h2 class="display-4">MM.Paysages</h2>
          <p class="lead">Entretien/élagage.</p>
        </div>
      </div>
      <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
  <div class="col-md-5">
    <h3><?php print_r("projet ".$row->nom .":"); ?></h3>
    <p> <center><?php print_r($row->description); ?></center></p>
    <a class="btn btn-primary" href="#">View Project</a>
  </div>
</div>
</div>
<div><hr></div>
<?php endforeach; ?>
<!-- /.row -->

<hr>


<hr>

<!-- Pagination -->
<ul class="pagination justify-content-center">
  <li class="page-item">
    <a class="page-link" href="#" aria-label="Previous">
      <span aria-hidden="true">&laquo;</span>
      <span class="sr-only">Previous</span>
    </a>
  </li>
  
  <li class="page-item">
    <a class="page-link" href="#">1</a>
  </li>
  <li class="page-item">
    <a class="page-link" href="#">2</a>
  </li>
  <li class="page-item">
    <a class="page-link" href="#">3</a>
  </li>
  <li class="page-item">
    <a class="page-link" href="#" aria-label="Next">
      <span aria-hidden="true">&raquo;</span>
      <span class="sr-only">Next</span>
    </a>
  </li>
</ul>

</div>
<!-- /.container -->
<style> 
  .carousel-item {
  height: 100vh;
  min-height: 350px;
  background: no-repeat center center scroll;
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
}
  </style>
