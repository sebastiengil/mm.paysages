<?php ?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <title>MM.Paysages</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="jumbotron.css" rel="stylesheet">
  </head>

  <body>

    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
      <a class="navbar-brand" href="#">MMPaysages</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="home">Acceuil <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="about">Mes projets</a>
          </li>
		  <li class="nav-item">
            <a class="nav-link" href="contact">contact</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="conseils">Conseil du mois</a>
          </li>
         </ul>
        
        <form class="form-inline my-2 my-lg-0 pull-right">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit"><a href='/Home/index'>Se déconnecter</a></button>
        </form>
      </div>
    </nav>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="assets/js/vendor/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
   

      <!-- Main jumbotron for a primary marketing message or call to action -->
      <div class="jumbotron" id="titlemm">
        <div class="container">
          <center><h2 class="display-3">MM.Paysages</h2></center>
        </div>
      </div>

    </main>

    <?php echo form_open('form'); ?>

<p>
  <?php echo form_label('Identifiant: ', 'login'); ?>
  <?php echo form_input('user_name', set_value('user_name'), 'class="form-control" id="login" autofocus'); ?>
</p>
<p>
  <?php echo form_label('Mot de passe:', 'password'); ?>
  <?php echo form_password('user_password', '', 'class="form-control" id="password"'); ?>
</p>
<p class="pull-right">
  <?php echo form_submit('send', 'Envoyer', 'class="btn btn-dark"'); ?>
</p>
<p>
  <?php echo form_close(); ?>
  <?php echo validation_errors(); ?>
</p>

<footer class="container">
      <p><center>&copy; MMPaysages 2014</center></p>
    </footer>
       <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="assets/js/vendor/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
  </body>
</html>


