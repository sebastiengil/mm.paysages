<?php ?>

    <main role="main">
      
      <!-- Main jumbotron for a primary marketing message or call to action -->
      <div class="jumbotron">
        <div class="container">
          <h2 class="display-3">Contactez moi</h2>
          <p>N'hésitez pas à m'envoyer un mail pour devis ou information, je me ferais un plaisir d'y répondre dans les plus brefs délais. Coordialement Mme Munoz.
		  </p>
      <?= form_open('contact'); ?>

        <div class="form-group">
            <label for="ID_email" class="text-left">E-mail</label>
            <input name="email" required type="email" class="form-control" id="ID_email" placeholder="Mettez votre email" value="<?= set_value('email'); ?>">
        </div>

        <div class="form-group">
            <label for="ID_email" class="text-left">Title</label>
            <input name="title" required type="text" class="form-control" id="ID_email" placeholder="Titre" value="<?= set_value('title'); ?>">
        </div>

        <div class="form-group">
            <label class="mb-0">Message</label>
            <textarea name="message" required class="form-control" id="ID_message" rows="16" placeholder="Entrez votre message ici"><?= set_value('message'); ?></textarea>
        </div>

        <div class="text-right">
            <button class="btn btn-primary">Envoyer</button>
        </div>

    </form>
		</div>
      </div>
