<?php
//applications/config/email.php

$config['protocol']     = 'smtp';
$config['smtp_host']    = 'mail.smtp_host.com';
$config['smtp_port']    = '465'; // 8025, 587 and 25 can also be used. Use Port 465 for SSL.
$config['smtp_crypto']  = 'ssl';
$config['smtp_user']    = 'username';
$config['smtp_pass']    = 'password';
$config['charset']      = 'utf-8';
$config['mailtype']     = 'html';
$config['newline']      = "\r\n";