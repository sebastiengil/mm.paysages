<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller{
	
	// function view($page = 'home')
	// {
	// 	if( !file_exists('application/views/pages/'.$page.'.php'))
	// 	{
	// 		show_404();
	// 	}
		
	// 	$this->load->view('pages/'.$page);
		
	// }

		function index(){
			$this->load->model('ModeleConseil');
			$data['query'] = $this->ModeleConseil->getConseils();
			$this->load->view('pages/includes/header');
			$this->load->view('pages/admin', $data);
			$this->load->view('pages/includes/footer');
		}
		public function all(){
        
			$this->load->model('ModeleConseil');
			$data['query'] = $this->ModeleConseil->getConseils();
			$this->load->view('pages/includes/header');
			$this->load->view('pages/admin', $data);
			$this->load->view('pages/includes/footer');
			
		}
}
 