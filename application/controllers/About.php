<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller{
    function index(){
        $this->load->view('pages/includes/header');
		$this->load->view('pages/about');
		$this->load->view('pages/includes/footer');
    }

    function detailProjets(){
        $this->load->view('pages/includes/header');
		$this->load->view('pages/detailProjets');
		$this->load->view('pages/includes/footer');
    }
    
}