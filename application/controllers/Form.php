<?php

class Form extends CI_Controller {

        public function index()
        {
                $this->load->helper(array('form', 'url'));

                $this->load->library('form_validation');

                $this->form_validation->set_rules('nom', 'Identifiant', 'required');
                $this->form_validation->set_rules('password', 'Mot de passe', 'required',
                        array('required' => 'You must provide a %s.')
                );

                if ($this->form_validation->run() == FALSE)
                {
                        $this->load->view('pages/myform');
                }
                else
                {
                        $this->load->view('includes/header');
                        $this->load->view('pages/formsuccess');
                        $this->load->view('includes/footer');
			
                }
        }
        
}