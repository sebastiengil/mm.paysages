<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');

class Conseil extends CI_Controller{

    function __construct()
    {
        parent::__construct();
 
        $this->load->model('modeleConseil');
        $this->load->helper('url_helper');
        $this->load->helper('form');
        $this->load->library()('form_validation');

    }

        public function all(){
        
        $data['conseils'] = $this->ModeleConseil->getConseils();
        $this->load->view('pages/includes/header');
        $this->load->view('pages/admin_conseil', $data);
        $this->load->view('pages/includes/footer');
        
    }

    public function conseil(){

        $this->load->model('ModeleConseil');
        $data['query'] = $this->ModeleConseil->getConseils();
        $this->load->view('pages/includes/header');
        $this->load->view('pages/conseils');    
        $this->load->view('pages/includes/footer');
    }

    function create(){
        
        $this->load->view('pages/includes/header');
        $this->load->view('pages/admin_conseil', $data);
        $this->load->view('pages/includes/footer');
    }

    public function edit($id){

        $id = $this->uri->segment(3);
        $data = array();

        if (empty($id)){
            show_404();
        }else{
            $data['conseil'] = $this->modeleConseil->get_conseils_by_id($id);
            $this->load->view('conseil/edit', $data);
        }
    }

    public function store(){

        $this->form_validation->set_rules('mois', 'mois', 'required');
        $this->form_validation->set_rules('conseil', 'conseil', 'required');
        
        $id = $this->input->post('id');

        if ($this->form_validation->run() === FALSE){

            if(empty($id)){
                redirect( base_url('conseil/create'));
            }else{
                redirect( base_url('conseil/edit/'.$id));
            }
        }else{

            $data['conseil'] = $this->modeleConseil->createOrUpdate();
            redirect( base_url('conseil'));
        }
    }

    public function delete(){
     
        $id = $this->uri->segment(3);

        if (empty($id)){
            shox_404();
        }
        $conseil = $this->ModeleConseil->delete($id);

        redirect( base_url('conseil'));
    }
            
}



