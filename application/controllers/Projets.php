<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');

class Projets extends CI_Controller{

    function __construct()
    {
        parent::__construct();

        $this->load->database();
        $this->load->helper('url');
 
        $this->load->library('grocery_CRUD');
 
    }

        public function all(){
        
        $this->load->model('ModeleProjet');
        $data['query'] = $this->ModeleProjet->getProjets();
        $this->load->view('pages/includes/header');
        $this->load->view('pages/about', $data);
        $this->load->view('pages/includes/footer');
        
    }
    
    public function projet()
    {
        $crud = new grocery_CRUD();
        $crud->set_table('projets');
       
        $output = $crud->render();
 
        $this->_example_output($output);        
    }
 
    function _example_output($output = null)
 
    {
        $this->load->view('pages/includes/header');
        $this->load->view('pages/admin_projet', $output);    
        $this->load->view('pages/includes/footer');
    }
}