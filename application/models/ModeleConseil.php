<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ModeleConseil extends CI_Model{


    function __construct(){
         //parent::__construct();
         $this->load->database();
    }
     
     
    public function getConseils(){
          
            $query = $this->db->get('conseils');

            // foreach ($query->result() as $row){
            
            //     $row->mois;
            //     $row->conseil;
               
            // }
            return $query->result();
    }
    public function get_conseils_by_id($id){

        $query = $this->db->get_where('conseils', array('id' => $id));
        return $query->row();
    }
      
    public function createOrUpdate(){

        $this->load->helper('url');
        $id = $this->input->post('id');
 
        $data = array(
            'mois' => $this->input->post('mois'),
            'conseil' => $this->input->post('conseil')
        );
        if (empty($id)) {
            return $this->db->insert('conseils', $data);
        } else {
            $this->db->where('id', $id);
            return $this->db->update('conseils', $data);
        }
    }
     
    public function delete($id){

        $this->db->where('id', $id);
        return $this->db->delete('conseil');
    }


}